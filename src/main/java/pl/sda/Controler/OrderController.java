package pl.sda.Controler;

import pl.sda.Service.MaterialService;
import pl.sda.Service.OrderService;
import pl.sda.model.Material;
import pl.sda.model.Order;
import pl.sda.model.OrderedProduct;

import java.util.List;

public class OrderController {

    private OrderService orderService;
    private MaterialService materialService;

    public OrderController(OrderService orderService, MaterialService materialService){
        this.orderService= orderService;
        this.materialService=materialService;
    }

    public void createOrder(Order order){
        List<OrderedProduct> orderedProducts = order.getOrderedProduct();

        if(correctOrderedProducts(orderedProducts)){
            orderService.save(order);
        }
    }

    public boolean correctOrderedProducts(List<OrderedProduct> orderedProducts) {
        for(OrderedProduct orderedProduct : orderedProducts){
           List<Material> possibleMaterials= materialService.getByProduct(orderedProduct.getProduct());
           if(!possibleMaterials.contains(orderedProduct.getMaterial())){
               return true;
               }
           }
        return false;
    }
}
