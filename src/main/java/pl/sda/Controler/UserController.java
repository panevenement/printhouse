package pl.sda.Controler;

import org.apache.commons.mail.EmailException;
import pl.sda.Service.UserService;
import pl.sda.Utils.UserValidation;
import pl.sda.exeption.UserEmailExistsException;
import pl.sda.model.User;

public class UserController {

    private UserService service;

    public UserController(UserService service){
        this.service=service;
    }

    public void register(User user) throws EmailException {
        if(UserValidation.isValid(user)) {
            try {
                service.register(user);
            } catch (UserEmailExistsException e) {
                //TODO: add logger and error message in view.
            }
        }
    }
}
