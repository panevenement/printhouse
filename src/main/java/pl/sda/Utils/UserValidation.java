package pl.sda.Utils;

import org.apache.commons.lang3.StringUtils;
import pl.sda.model.User;

public class UserValidation {

    private static final String EMAIL_REGEX = "^[a-zA-Z0-9_!#$%&‘*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

    public static boolean isValid(User user){
        return hasOnlyLetter(user.getFirstName())
                && hasOnlyLetter(user.getLastName())
                && user.getEmail().matches(EMAIL_REGEX)
                && isValidPassword(user.getPassword());

    }

    private static boolean hasOnlyLetter(String value){
        return StringUtils.isNoneBlank(value)
                && value.matches("[a-zA-Z]");
    }

    private static boolean isValidPassword(String password){
        return StringUtils.isNoneBlank(password)
                && password.length() >=8
                && password.matches("[0-9]");
    }
}
