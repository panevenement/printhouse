package pl.sda.email;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import pl.sda.Configurations.Config;
import pl.sda.model.Order;
import pl.sda.model.OrderedProduct;
import pl.sda.model.User;

import java.util.Arrays;
import java.util.stream.Collector;

public class emailSender {


    public void sendMail() throws EmailException {
        String reciver = "xxx.xxx@icloud.com";
        String title = "Hello World";
        String content = "Email content";

        Email email = new SimpleEmail();
        email.setFrom("xxx.xxx@icloud.com");
        email.setSubject(title);
        email.setMsg(content);
        email.addTo(reciver);
        email.setHostName(Config.HOST);
        email.setSmtpPort(Config.PORT);
        email.setAuthentication(Config.USER_LOGIN,Config.USER_PASSWORD);
        email.setSSLOnConnect(true);
        email.send();

    }

    public void sendCostumerUser(User user) throws EmailException{
        String reciver = user.getEmail();
        String title = "Witaj! " +user.getFirstName();
        String hidden_password = user.getPassword();
        String content = "Twoje hasło to:" +user.hiddenPassword(hidden_password);



        Email email = new SimpleEmail();
        email.setFrom("xxx.xxx@icloud.com");
        email.setSubject(title);
        email.setMsg(content);
        email.addTo(reciver);
        email.setHostName(Config.HOST);
        email.setSmtpPort(Config.PORT);
        email.setAuthentication(Config.USER_LOGIN,Config.USER_PASSWORD);
        email.setSSLOnConnect(true);
        email.send();

    }
    public void sendOrderInformation(Order order) throws EmailException{

        String reciver = order.getUser().getEmail();
        String title = "Witaj! " +order.getUser().getFirstName();
        String content = "Twoje zamówienie to: \n";
        for(int i=0; i<order.getOrderedProduct().size();i++) {
            content += order.getOrderedProduct().get(i).getProduct() +" ";
            content += order.getOrderedProduct().get(i).getMaterial() +" ";
            content += order.getOrderedProduct().get(i).getQuantity() +"\n";

        }

        Email email = new SimpleEmail();
        email.setFrom("xx.xxx@icloud.com");
        email.setSubject(title);
        email.setMsg(content);
        email.addTo(reciver);
        email.setHostName(Config.HOST);
        email.setSmtpPort(Config.PORT);
        email.setAuthentication(Config.USER_LOGIN,Config.USER_PASSWORD);
        email.setSSLOnConnect(true);
        email.send();
    }



}
