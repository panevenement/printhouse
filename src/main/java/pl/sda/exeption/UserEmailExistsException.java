package pl.sda.exeption;

public class UserEmailExistsException extends Exception {

    public UserEmailExistsException(String message){
        super(message);
    }
    public UserEmailExistsException(){
    }
}
