package pl.sda.model;


import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Order {

    private int id;
    private User user;
    private OrderStatus status;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private Address deliveryAddress;
    private List<OrderedProduct> orderedProduct;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(LocalDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public List<OrderedProduct> getOrderedProduct() {
        return orderedProduct;
    }

    public void setOrderedProduct(List<OrderedProduct> orderedProduct) {
        this.orderedProduct = orderedProduct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getId() == order.getId() &&
                Objects.equals(getUser(), order.getUser()) &&
                getStatus() == order.getStatus() &&
                Objects.equals(getCreatedDate(), order.getCreatedDate()) &&
                Objects.equals(getModifiedDate(), order.getModifiedDate()) &&
                Objects.equals(getDeliveryAddress(), order.getDeliveryAddress()) &&
                Objects.equals(getOrderedProduct(), order.getOrderedProduct());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getUser(), getStatus(), getCreatedDate(), getModifiedDate(), getDeliveryAddress(), getOrderedProduct());
    }
}
