package pl.sda.model;

import java.util.Objects;

public class Material {

    private int id;
    private String name;
    private String description;
    private double multiplier;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Material)) return false;
        Material material = (Material) o;
        return getId() == material.getId() &&
                Double.compare(material.getMultiplier(), getMultiplier()) == 0 &&
                Objects.equals(getName(), material.getName()) &&
                Objects.equals(getDescription(), material.getDescription());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getName(), getDescription(), getMultiplier());
    }
}
