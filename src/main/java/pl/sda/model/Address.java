package pl.sda.model;

public class Address {

    private int id;
    private String city;
    private String strett;
    private String zipCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStrett() {
        return strett;
    }

    public void setStrett(String strett) {
        this.strett = strett;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
