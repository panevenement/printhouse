package pl.sda.Service;

import pl.sda.Repository.ProductRepository;
import pl.sda.model.Material;
import pl.sda.model.Product;

import java.util.List;

public class ProductService {

    private ProductRepository repository;

    public ProductService(ProductRepository repository){
        this.repository=repository;
    }

    public List<Product> getAll(){
        return repository.getAll();
    }
    public Product getProductByID(int id){
        return repository.getProductByID(id);
    }


}
