package pl.sda.Service;

import org.apache.commons.mail.EmailException;
import pl.sda.Repository.UserRepository;
import pl.sda.email.emailSender;
import pl.sda.exeption.UserEmailExistsException;
import pl.sda.model.User;

import java.util.Optional;

public class UserService {

    private UserRepository repository;
    private emailSender emailSender;

    public UserService(UserRepository repository){
        this.repository=repository;
    }

    public void register(User user) throws UserEmailExistsException, EmailException {
        Optional<User> userFromDb = repository.getUserByEmail(user.getEmail());
        if(userFromDb.isPresent()) {
            throw new UserEmailExistsException(user.getEmail());
        }

        repository.save(user);
        emailSender.sendCostumerUser(user);

    }
    public void update(User user){
        repository.update(user);
    }
    public Optional<User> getUserByID(int userID){
        return repository.getUserByID(userID);
    }



}
