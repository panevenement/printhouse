package pl.sda.Service;

import pl.sda.Repository.MaterialRepository;
import pl.sda.model.Material;
import pl.sda.model.Product;

import java.util.List;

public class MaterialService {

    private MaterialRepository repository;

    public MaterialService(MaterialRepository repository){
        this.repository=repository;
    }

    public List<Material> getAll(){
        return repository.getAll();
    }
    public Material getByMaterialID(int id){
        return repository.getMaterialByID(id);
    }
    public List<Material> getByProduct(Product product){
        return repository.getByProductID(product.getId());
    }

}
