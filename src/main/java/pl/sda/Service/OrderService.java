package pl.sda.Service;

import pl.sda.Repository.OrderRepository;
import pl.sda.model.Order;
import pl.sda.model.OrderStatus;

import java.util.List;

public class OrderService {

    private OrderRepository repository;

    public OrderService(OrderRepository repository){
        this.repository=repository;
    }
    public void save(Order order){

        repository.save(order);
    }
    public List<Order> getByUserID(int id){
        return repository.getByUserID(id);
    }
    public List<Order> getAll(){
        return repository.getAll();
    }
    public Order getByID(int id){
        return repository.getByID(id);
    }


}
